# Create React App example

## How to use

Install it and run:

```sh
npm install
npm start
```

## Note from developer 
1. Add sufficient unit tests for the functionality of filtering and listing (Not yet done!)
2. Write clean code (✅)
3. Application should be responsive (✅)
4. No backend is needed (✅)
5. Some improvement on UI/UX (could be done but not yet arrange time)

## The idea behind the example

This example demonstrates how you can use [Create React App](https://github.com/facebookincubator/create-react-app). It includes `@material-ui/core` and its peer dependencies, including `emotion`, the default style engine in Material-UI v5. If you prefer, you can [use styled-components instead](https://next.material-ui.com/guides/interoperability/#styled-components).
