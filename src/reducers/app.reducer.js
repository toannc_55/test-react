import cst from '../constants';

const initialState = { errorMessage: '', loading: false, recipes: [] };

export const app = (state = initialState, action) => {
  switch (action.type) {
    case cst.ACTION_ADD_RECIPE:
      return {
        ...state,
        recipes: state.recipes.concat(action.payload)
      };
    default:
      return state;
  }
};
