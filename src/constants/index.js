const cst = {
  ACTION_LOADING: 'loading',
  ACTION_LOADED: 'loaded',
  ACTION_ERROR_OCCURS: 'err.occur',
  ACTION_CLEAR_ERRORS: 'err.clear',
  ACTION_ADD_RECIPE: 'ACTION-ADD-RECIPE'
};

export default cst;
