import React from 'react'
import { colors } from '@material-ui/core'
import ListItem from '@material-ui/core/ListItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import {
  ExpandLess, ExpandMore, PlusOne, QuestionAnswer,
} from '@material-ui/icons'
import LabelIcon from '@material-ui/icons/Label';
import ListItemText from '@material-ui/core/ListItemText'
import Collapse from '@material-ui/core/Collapse'
import List from '@material-ui/core/List'
import ListItemAvatar from '@material-ui/core/ListItemAvatar'
import Avatar from '@material-ui/core/Avatar'
import NewIngredient from '../NewIngredient'
import Tooltip from '@material-ui/core/Tooltip'

const IngredientList = (props) => {
  const [openIngredientList, setOpenIngredientList] = React.useState(false)
  const [openNewIngr, setOpenNewIngr] = React.useState(false)
  
  const { data, setData } = props
  const ingredient = data.ingredient
  const handleOpenIngredientList = (e) => {
    if (!openIngredientList) setOpenNewIngr(false)
    setOpenIngredientList(!openIngredientList)
  }
  
  const handleOpenNewIngr = () => {
    setOpenNewIngr(true)
    setOpenIngredientList(false)
  }
  
  const handleCloseNewIngr = () => {
    setOpenNewIngr(false)
  }
  
  return (
    <>
      <ListItem
        button
      >
        <ListItemIcon
          onClick={handleOpenIngredientList}
        >
          <QuestionAnswer />
        </ListItemIcon>
        <ListItemText
          onClick={handleOpenIngredientList}
          primary="Ingredient List"
        />
        {openIngredientList ? <ExpandLess /> : <ExpandMore />}
        <Tooltip title="Click here to add new ingredient">
          <PlusOne color="primary" onClick={handleOpenNewIngr} />
        </Tooltip>
      </ListItem>
      <Collapse
        in={openIngredientList}
        timeout="auto"
        unmountOnExit
      >
        <List>
          {ingredient.map((v, index) => (
            <ListItem
              key={index + v.toString()}
              button
            >
              <ListItemAvatar>
                <Avatar>
                  <LabelIcon />
                </Avatar>
              </ListItemAvatar>
              <ListItemText primary={`${v.qty} ${v.unit} ${v.name}`} />
            </ListItem>
          ))}
        </List>
      </Collapse>
      {openNewIngr && (
        <NewIngredient
          handleCloseNewIngr={handleCloseNewIngr}
          data={data}
          setData={setData}
          handleOpenIngredientList={handleOpenIngredientList}
        />
      )}
    </>
  )
}

export default IngredientList
