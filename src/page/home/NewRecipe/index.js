import React from 'react';
import { connect } from 'react-redux'
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogContent from '@material-ui/core/DialogContent';
import MuiDialogActions from '@material-ui/core/DialogActions';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import CloseIcon from '@material-ui/icons/Close';
import Typography from '@material-ui/core/Typography';
import { useSnackbar } from 'notistack';
import Divider from '@material-ui/core/Divider';
import Grow from '@material-ui/core/Grow'
import TextField from '@material-ui/core/TextField'
import makeStyles from '@material-ui/core/styles/makeStyles'
import Grid from '@material-ui/core/Grid'
import IngredientList from './IngredientList'
import { appAction } from '../../../actions'

const styles = (theme) => ({
  root: {
  },
  /*  nested: {
    paddingLeft: theme.spacing(4),
  }, */
  closeButton: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500],
  },
});

const DialogTitle = withStyles(styles)((props) => {
  const {
    children, classes, onClose, ...other
  } = props;
  return (
    <MuiDialogTitle disableTypography className={classes.root} {...other}>
      <Typography variant="h6">{children}</Typography>
      {onClose ? (
        <IconButton aria-label="close" className={classes.closeButton} onClick={onClose}>
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

const Transition = React.forwardRef((props, ref) => <Grow direction="up" ref={ref} {...props} />);

const DialogContent = withStyles((theme) => ({
}))(MuiDialogContent);

const DialogActions = withStyles((theme) => ({
}))(MuiDialogActions);

const useStyles = makeStyles((theme) => ({
  nested: {
    paddingLeft: theme.spacing(4),
  },
  submitRecipe: {
    float: 'right',
  },
}));

const NewRecipe = (props) => {
  // const { enqueueSnackbar } = useSnackbar();
  const classes = useStyles()
  const { handleClose, open, addRecipe } = props;
  const [data, setData] = React.useState({
    name: '',
    step: '',
    image: '',
    ingredient: [],
  })

  const handleChange = (e) => {
    setData({ ...data, [e.target.name]: e.target.value })
  }

  const handleSubmit = (e) => {
    e.preventDefault()
    addRecipe(data)
    handleClose()
  }

  return (
    <Dialog
      onClose={handleClose}
      aria-labelledby="new-project"
      open={open}
      TransitionComponent={Transition}
      keepMounted
    >
      <DialogTitle id="new-project" onClose={handleClose}>
        CREATE NEW RECIPE
      </DialogTitle>
      <Divider />
      <DialogContent>
        <Grid
          container
          spacing={4}
        >
          <Grid
            id="main-form"
            item
            xs={12}
            component="form"
            onSubmit={handleSubmit}
          >
            <Grid container>
              <Grid item xs={6}>
                <TextField
                  label="Recipe name"
                  required
                  variant="standard"
                  name="name"
                  value={data.name}
                  onChange={handleChange}
                />
              </Grid>
              <Grid item xs={6}>
                <TextField
                  type="url"
                  label="Image"
                  required
                  variant="standard"
                  name="image"
                  value={data.image}
                  onChange={handleChange}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  label="Step to cook"
                  required
                  variant="standard"
                  name="step"
                  value={data.step}
                  onChange={handleChange}
                />
              </Grid>
            </Grid>
          </Grid>
          <Grid item xs={12}>
            <IngredientList
              data={data}
              setData={setData}
            />
          </Grid>
          <Grid item xs={12}>
            <Button
              className={classes.submitRecipe}
              size="medium"
              variant="contained"
              type="submit"
              form="main-form"
            >
              Create New Recipe
            </Button>
          </Grid>
        </Grid>
      </DialogContent>
      <DialogActions />
    </Dialog>
  );
}

const mapDispatchToProps = (dispatch) => ({
  addRecipe: (recipe) => dispatch(appAction.addRecipe(recipe)),
})

export default connect(undefined, mapDispatchToProps)(NewRecipe)
