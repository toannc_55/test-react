import React from 'react'
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'
import Box from '@material-ui/core/Box'
import { makeStyles } from '@material-ui/core'
import Grid from '@material-ui/core/Grid'

const defaultState = {
  name: '',
  qty: 0,
  unit: '',
}

const useStyles = makeStyles((theme) => ({
  btn: {
    float: 'right'
  },
}))

const NewIngredient = (props) => {
  const { data, setData, handleOpenIngredientList } = props
  const classes = useStyles()
  const [ingredient, setIngredient] = React.useState(defaultState)

  const handleChange = (e) => {
    setIngredient({ ...ingredient, [e.target.name]: e.target.value })
  }

  const handleSubmit = () => {
    data.ingredient.push(ingredient)
    setIngredient(defaultState)
    setData(data)
    handleOpenIngredientList()
  }

  return (
    <Grid
      container
      spacing={2}
      component="form"
      onSubmit={handleSubmit}
    >
      <Grid item xs={4}>
        <TextField
          label="name"
          required
          variant="standard"
          name="name"
          value={ingredient.name}
          onChange={handleChange}
        />
      </Grid>
      <Grid item xs={4}>
        <TextField
          label="Quantity"
          required
          variant="standard"
          name="qty"
          value={ingredient.qty}
          onChange={handleChange}
          type="number"
        />
      </Grid>
      <Grid item xs={4}>
        <TextField
          label="Unit"
          required
          variant="standard"
          name="unit"
          value={ingredient.unit}
          onChange={handleChange}
        />
      </Grid>
      <Grid xs={12} item>
        <Button
          className={classes.btn}
          type="submit"
          variant="contained"
        >
          Add
        </Button>
      </Grid>
    </Grid>
  )
}

export default NewIngredient
