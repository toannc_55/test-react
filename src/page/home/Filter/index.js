import React from 'react'
import FormControl from '@material-ui/core/FormControl'
import InputLabel from '@material-ui/core/InputLabel'
import Select from '@material-ui/core/Select'
import { connect } from 'react-redux'
import makeStyles from '@material-ui/core/styles/makeStyles'
import MenuItem from '@material-ui/core/MenuItem'
import Checkbox from '@material-ui/core/Checkbox'
import ListItemText from '@material-ui/core/ListItemText'
import Input from '@material-ui/core/Input'
import {getIngredients} from '../index'

const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
    maxWidth: 300,
  },
  chips: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  chip: {
    margin: 2,
  },
  noLabel: {
    marginTop: theme.spacing(3),
  },
}));

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};

function shorten(s) {
  if (s.length > 30)
    return s.slice(0, 30) + '...'
  return s
}

const Filter = (props) => {
  const { recipes, selectIngredients, setSelectIngredients } = props
  const classes = useStyles()
  
  const ingredients = getIngredients(recipes)

  const handleChange = (event) => {
    setSelectIngredients(event.target.value);
  };

  return (
    <FormControl className={classes.formControl}>
      <InputLabel id="demo-mutiple-checkbox-label">Filter by ingredients</InputLabel>
      <Select
        labelId="demo-mutiple-checkbox-label"
        id="demo-mutiple-checkbox"
        multiple
        value={selectIngredients}
        onChange={handleChange}
        input={<Input />}
        renderValue={(selected) => shorten(selected.join(', '))}
        MenuProps={MenuProps}
      >
        {ingredients.map((ingredient) => (
          <MenuItem key={ingredient} value={ingredient}>
            <Checkbox checked={selectIngredients.indexOf(ingredient) > -1} />
            <ListItemText primary={ingredient} />
          </MenuItem>
        ))}
      </Select>
    </FormControl>
  )
}

const mapStateToProps = (state) => ({
  recipes: state.app.recipes,
})

export default connect(mapStateToProps)(Filter)
