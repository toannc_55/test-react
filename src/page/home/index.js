import React from 'react';
import { connect } from 'react-redux'
import AppBar from '@material-ui/core/AppBar';
import Button from '@material-ui/core/Button';
import CameraIcon from '@material-ui/icons/PhotoCamera';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Grid from '@material-ui/core/Grid';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import AddIcon from '@material-ui/icons/Add'
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Link from '@material-ui/core/Link';
import NewRecipe from './NewRecipe/index.js'
import Search from './Search'
import Filter from './Filter'

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
      <Link color="inherit" href="https://material-ui.com/">
        Your Website
      </Link>
      {' '}
      {new Date().getFullYear()}
      .
    </Typography>
  );
}

const useStyles = makeStyles((theme) => ({
  icon: {
    marginRight: theme.spacing(2),
  },
  heroContent: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(8, 0, 6),
  },
  heroButtons: {
    marginTop: theme.spacing(4),
  },
  cardGrid: {
    paddingTop: theme.spacing(8),
    paddingBottom: theme.spacing(8),
  },
  card: {
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
  },
  cardMedia: {
    paddingTop: '56.25%', // 16:9
  },
  cardContent: {
    flexGrow: 1,
  },
  footer: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(6),
  },
}));

const format = (ingredient) => {
  let result = ingredient.map((v) => `${v.qty} ${v.unit} ${v.name}`).join(',')
  if (result.length > 30) result = `${result.slice(0, 30)}...`
  return result;
}

const hasIngredients = (recipe, selectIngredients) => {
  const ingredients = recipe.ingredient.map((v) => v.name);
  const intersect = ingredients.filter((value) => selectIngredients.includes(value));
  return intersect.length > 0
}

export const getIngredients = (recipes) => {
  const s = new Set()
  recipes.forEach((recipe) => recipe.ingredient.forEach((ingr) => s.add(ingr.name)))
  return Array.from(s)
}

const Home = (props) => {
  const classes = useStyles();
  const { recipes } = props;
  
  const [open, setOpen] = React.useState(false);
  const [search, setSearch] = React.useState()
  const [selectIngredients, setSelectIngredients] = React.useState(getIngredients(recipes));
  
  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };

  return (
    <>
      <NewRecipe
        open={open}
        handleClickOpen={handleClickOpen}
        handleClose={handleClose}
      />
      <AppBar position="relative">
        <Toolbar>
          <CameraIcon className={classes.icon} />
          <Search
            search={search}
            setSearch={setSearch}
          />
        </Toolbar>
      </AppBar>
      <main>
        {/* Hero unit */}
        <div className={classes.heroContent}>
          <Container maxWidth="sm">
            <Typography component="h1" variant="h2" align="center" color="textPrimary" gutterBottom>
              Recipes for Life
            </Typography>
            <div className={classes.heroButtons}>
              <Grid container spacing={2} justify="center">
                <Grid item>
                  <Button
                    variant="contained"
                    color="primary"
                    onClick={handleClickOpen}
                    endIcon={<AddIcon />}
                  >
                    Add new recipe
                  </Button>
                  <Grid
                    item
                    variant="contained"
                    color="primary"
                  >
                    <Filter
                      selectIngredients={selectIngredients}
                      setSelectIngredients={setSelectIngredients}
                    />
                  </Grid>
                </Grid>
              </Grid>
            </div>
          </Container>
        </div>
        <Container className={classes.cardGrid} maxWidth="md">
          {/* End hero unit */}
          <Grid container spacing={4}>
            {recipes
              .filter((recipe) => !search || recipe.name && recipe.name.toLowerCase().includes(search.toLowerCase()))
              .filter((recipe) => hasIngredients(recipe, selectIngredients))
              .map((recipe) => (
                <Grid item key={recipe.toString()} xs={12} sm={6} md={4}>
                  <Card className={classes.card}>
                    <CardMedia
                      className={classes.cardMedia}
                      image={recipe.image || 'https://source.unsplash.com/random'}
                      title="Image title"
                    />
                    <CardContent className={classes.cardContent}>
                      <Typography gutterBottom variant="h5" component="h2">
                        {recipe.name}
                      </Typography>
                      <Typography>
                        {recipe.step}
                      </Typography>
                      <Typography color="textSecondary">
                        {format(recipe.ingredient)}
                      </Typography>
                    </CardContent>
                    <CardActions>
                      <Button size="small" color="primary">
                        View
                      </Button>
                      <Button size="small" color="primary">
                        Edit
                      </Button>
                    </CardActions>
                  </Card>
                </Grid>
              ))}
          </Grid>
        </Container>
      </main>
      {/* Footer */}
      <footer className={classes.footer}>
        <Typography variant="h6" align="center" gutterBottom>
          Footer
        </Typography>
        <Typography variant="subtitle1" align="center" color="textSecondary" component="p">
          Something here to give the footer a purpose!
        </Typography>
        <Copyright />
      </footer>
      {/* End footer */}
    </>
  );
}

const mapStateToProps = (state) => ({
  recipes: state.app.recipes,
})

export default connect(mapStateToProps)(Home)
